package rmiraspberry.api;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface SoundSensor extends Remote {
     public  ArrayList<String> obtenerRuiodo () throws RemoteException,
             IOException, Exception;
     
}