package rmiraspberry.client;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Iterator;
import java.util.Scanner;
import rmiraspberry.api.SoundSensor;

public class Main {

    public static void main(String[] args) throws RemoteException, NotBoundException, IOException, Exception {

        String host = "172.17.23.221";
        //vrString host = "localhost";

        String name = "Sensor";
        int num = 0;

        // Especificación del nombre del host donde se ejecuta el registro
        // y el puerto.
        Registry registry = LocateRegistry
                .getRegistry(host, 1028);

        // Buscar el servicio en el registro.
        SoundSensor ruido = (SoundSensor) registry.lookup(name);
 
        //Menú para la obtención de los datos provenientes del servidor
        System.out.println("\t\tMenu");
        System.out.println("**************************************************");
        System.out.println("Ingrese un numero de la opción");
        System.out.println("**************************************************");
        System.out.println("1. Niveles de ruido labiratorio de datos\n2. Salir");
        System.out.println("***************************************************");
        Scanner teclado = new Scanner(System.in);
        num = teclado.nextInt();
        System.out.println("***************************************************");
        switch (num) {
            case 1:
                System.out.println("Nivel de ruido en el laboratorio de datos (UTPL)");
                Iterator<String> lista = ruido.obtenerRuiodo().iterator();
                while (lista.hasNext()) {
                    System.out.println("Reported sound level: " + ruido.obtenerRuiodo());
                }
                break;
            case 2:
                System.exit(0);
                break;
            default:
                System.out.println("Opción incorrecta");
                break;
        }

    }
}
