package rmiraspberry.server;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
//import org.iot.raspberry.grovepi.GrovePi;
//import org.iot.raspberry.grovepi.pi4j.GrovePi4J;
import rmiraspberry.api.SoundSensor;

public class Main {

    public static void main(String[] args) throws RemoteException, IOException {
        String name = "Sensor";
        Registry registry = LocateRegistry.createRegistry(1028);

        //Implementación del objeto del sevidor
        SoundSensor greeting = new SoundSensorObject();

        //stup para el objeto de servidor en tiempo de ejecución de RMI
        //Implementa la comunicación requerida con el objeto servidor remoto
        //El cliente va a utilizar ese stup para invocar de forma transparente
        //el método en el objeto servidor
        SoundSensor stub = (SoundSensor) UnicastRemoteObject.exportObject(greeting, 0);

        registry.rebind(name, stub);
        System.out.println("Server is running...");
    }
}
