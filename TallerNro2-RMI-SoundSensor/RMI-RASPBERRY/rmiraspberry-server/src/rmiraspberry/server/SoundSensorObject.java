package rmiraspberry.server;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.iot.raspberry.grovepi.GrovePi;
import org.iot.raspberry.grovepi.devices.GroveSoundSensor;
import org.iot.raspberry.grovepi.pi4j.GrovePi4J;
import rmiraspberry.api.SoundSensor;

public class SoundSensorObject implements SoundSensor {

    // Creación de un objeto de tipo GrovePi
    GrovePi grovePi;

    // Inicialización del obejto creado, mediante el constructor de la clase
    public SoundSensorObject() throws IOException {
        grovePi = new GrovePi4J();
    }

    // Implementación de la lógica de la aplicación
    @Override
    public ArrayList<String> obtenerRuiodo() throws RemoteException, IOException, 
            Exception {
        //Creación del objeto soundSensor de tipo GroveSoundSensor
        GroveSoundSensor soundSensor = new GroveSoundSensor(grovePi, 0);
        ArrayList<String> lstDatos = new ArrayList<>();

        //Captura la hora y fecha actual del servidor
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat hour = new SimpleDateFormat("HH:mm:ss");
        Thread.sleep(1000);

        // Obtiene el nivel del ruido en decibeles, convirtiendo su valor, de 
        //tipo String, y agregando a la lista, que sera solicitada por el cliente
        double soundLevel = soundSensor.get();
        String stringDouble = String.valueOf(soundLevel);
        lstDatos.add(stringDouble + " Fecha: " + date.format(now) + " Hora: " + 
                hour.format(now));
        return lstDatos;

    }

    
    

}
